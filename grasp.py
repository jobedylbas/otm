#!/usr/bin/env python3
import sys, os
import numpy as np
import itertools
import random 
import math
import time

class mttp:
    def __init__(self, matrix: list, max_games: int):
        """ Class that instantiate a mttp problem 
        """
        self.init_matrix = matrix           # Initial matrix with distance between teams
        self.curr_matrix = matrix           # Current matrix with distance between teams
        self.n_teams     = len(matrix)      # Number of teams
        self.n_games     = self.n_teams/2   # Number of games p/ round
        self.n_rounds    = self.n_games-1   # Number of rounds p/ half
        self.best_dist   = sys.maxsize      # Best distance found
        self.max_games   = max_games        # Number of team max games out
        self.team_hist   = np.zeros((self.max_games, self.n_teams), dtype= int) # Historic of in ou out games

    def dist_cities(self, t: tuple) -> tuple:
        """Return a tuple of type (citie1, citie2, distance) where
        the distance is between citie1 to citie2
        :param t: (citi1, citie2)
        :returns: a tuple like (citie1, citie2, distance)
        """
        return t + (self.curr_matrix[t[0]][t[1]], )

    def greedRandomized(self, greedness: float, seed: int) -> list:
        """Return a solution for mttp problem if found
        :param greedness: Randomization percentage to choose a game
        :param max_games: Number of games that team could play out
        :return: Set with the first half of the tournament
        """
        s = [] # Solution found
        i = 0  # index
        counter_repeated = 0 # Count how many times we get the same game
        last_c = 0           # Last element caught to evaluate 
        comeback_dist = 0    # If team need to comeback to home to play
        total_round_dist = 0 # Total distance of round
        round_games = []     # Games in the current round
        first_round = True   # Mean if is the first round
        teams_played = np.zeros(self.n_teams, dtype= int) # Teams that played on this round
        self.team_hist = np.zeros((self.max_games, self.n_teams), dtype= int) # Historic of in ou out games
        
        C = list(itertools.permutations(list(range(0, self.n_teams)), 2)) # Create list of games
        C = list(map(self.dist_cities, C)) # Create the list of games with distance of t1 to t2
        C = sorted(C, key= lambda t: t[::-1]) # Sort the games by the minimun distance between the two teams    
        
        while C:
            comeback_dist = 0 # Distance for the team comeback to home
            random.seed(None)
            # seed += 1
            if greedness == 0.0:
                try: c = C[i] # Get the element with the shortest path
                except:
                    return [] # Not feasible solution
            elif greedness <= 1.0 and greedness > 0.0:
                try: c = C[random.randint(0, int(len(C)*greedness))] # Get random element betwenn 0 and %len(C) greed 
                except: continue # Avoid index out of range passing to next iteration
            else:
                raise ValueError('Greedness must be between 0.0 and 1.0')
            
            # If the get random get stuck
            if last_c == c: 
                counter_repeated += 1
                if counter_repeated == 30:
                    return [] # Not feasible solution
            last_c = c

            # Check problem constraints
            if (not teams_played[c[0]] and not teams_played[c[1]] and # Teams that already played in this round
                self.team_hist[0][c[0]] + self.team_hist[1][c[0]] + self.team_hist[2][c[0]] < self.max_games):

                if self.team_hist[0][c[1]] + self.team_hist[1][c[1]] + self.team_hist[2][c[1]] == 0 and not first_round:
                    return []
                # The historic of team 1 must be less than three to be possible to 
                # play outside one more time
                
                # Check if team 2 played out in the last round to
                # come back home to play in this round
                if s:
                    game = list(filter(lambda t: t[0] == c[1], s[len(s)-1][0]))
                    if game:
                        comeback_dist = game[0][2]
                
                teams_played[c[0]] = 1 # t1 and t2 played this round
                teams_played[c[1]] = 1

                self.team_hist[len(s)%3][c[0]] = 1 # t1 played out 
                self.team_hist[len(s)%3][c[1]] = 0 # t2 played in

                round_games.append(c) # Add game in the current round
                C.remove(c) # Remove game from all games

                C = list(filter(lambda t: not (t[0] == c[1] and t[1] == c[0]), C)) # Remove mirrored games
                total_round_dist += comeback_dist+c[2]
                i = 0
                counter_repeated = 0
            elif greedness == 0:
                i += 1

            # Round ended
            if sum(teams_played) == self.n_teams:
                teams_played = np.zeros(self.n_teams, dtype= int) # None of teams played
                s.append([round_games, total_round_dist]) # Solution has a new round
                total_round_dist = 0
                round_games = [] # Round has be reset
                i = 0
                first_round = False
                # Reevaluate C on new locations of teams
                last_round = s[len(s)-1][0] # Get all games on last round
                for game in last_round: 
                    for c in C:
                        if c[0] == game[0]: # If team out played out on the last round
                            new_tuple = (c[0], c[1], self.curr_matrix[game[1]][c[1]]) # Team out has a new distance from 
                            C.append(new_tuple)                                       # the next game
                            C.remove(c)
                C = sorted(C, key= lambda t: t[::-1]) # Sort from the short to bigger path
                # print(S)
                # print(team_hist)
        return self.create_sec_half(s)

    def create_sec_half(self, s: list) -> list:
        """Create second half mirrored by the first one
        :param s: solution found for the first round
        :returns: a solution with two rounds if possible
        """
        n_rounds = len(s)
        r = []
        for i in range(0, n_rounds):
            r = []
            total_round_cost = 0
            for game in s[i][0]:
                comeback_dist = 0
                # game[0] -> Team that played outside and has to play in
                # game[1] -> Team that played in and has to play out
                
                # Check if the team that play out has a sequence of 3 three games out in a row

                new_t1_hist = self.team_hist[0][game[1]] + self.team_hist[1][game[1]] + self.team_hist[2][game[1]]
                new_t2_hist = self.team_hist[0][game[0]] + self.team_hist[1][game[0]] + self.team_hist[2][game[0]]
                if new_t1_hist == self.max_games or new_t2_hist == 0:
                    return [] # Not faiseble solution


                else:
                    self.team_hist[len(s)%3][game[1]] = 1 # t1 played out 
                    self.team_hist[len(s)%3][game[0]] = 0 # t2 played in
                    
                    # If team that play in has to come back home
                    game_t0_out = list(filter(lambda t: t[0] == game[0], s[len(s)-1][0]))
                    if game_t0_out:
                        comeback_dist = game_t0_out[0][2]
                    
                    # Get the last location of the team that play out
                    last_location = list(filter(lambda g: g[0] == game[1], s[len(s)-1][0]))
                    if not last_location:
                        last_location = game[1]
                    else:
                        last_location = last_location[0][1]

                    dist_to_game = self.curr_matrix[last_location][game[0]]
                    r.append((game[1], game[0], dist_to_game))
                    total_round_cost += dist_to_game+comeback_dist
            
            s.append([r, total_round_cost])

        return s

    def eval(self, s: list) -> int:
        """Evaluate a solution, a tournament game scheme and
        return the total distance reached by the teams
        :param S: tournament game scheme
        :returns: total distance
        """
        dist = 0
        if s:
            for r in s:
                dist += r[len(r)-1]
            
            # Calculate the return of last round to home
            for game in s[len(s)-1][0]:
                dist += self.curr_matrix[game[1]][game[0]]
            return dist
        return sys.maxsize

    def getBestValue(self):
        return self.best_dist

    def grasp(self, maxIterations: int, greedness: float) -> list:
        """GRASP for MTTP problem
        :param maxIterations: Number of max iterations of compute
        :param greedness: Randomization percentage
        :param nGames: Number of games per round
        :returns: return a list with best solution already found
        """
        S = [] # set of solutions
        s = [] # current best solution

        # Create maxIterations solutions
        for i in range(0, maxIterations):
            new_s = self.greedRandomized(greedness, i)
            
            # Append only new and fisable solution
            if new_s and not (new_s in S): 
                # print(new_s)
                S.append(new_s)
            else:
                continue # Avoid put empty solutions on solution set
            _s = self.localSearch(S) # Found the best solution
            
            _s_dist = self.eval(_s) # Evaluate the best solution
            
            # Check if the best solution known is the best solution
            if _s_dist < self.best_dist:
                s = _s
                self.best_dist = _s_dist
        # print(self.best_dist)
        return s
    
    def localSearch(self, S: list) -> list:
        """ Find the best solution from the already in 
        solutions set S.
        :param S: Set of solutions
        :returns: Best solution in the set S
        """
        best_s = []
        if S:
            # Found the best solution in the set
            for s in S:
                if self.eval(s) < self.best_dist:
                    best_s = s
            return best_s
        else:
            raise ValueError("The solution set S cannot be empty.") 

def tests():
    filenames   = ['N4', 'N6', 'N8', 'N10', 'N12', 'N14', 'C6', 'C8', 'C10', 'C12']
    best_scores = [8276, 26588, 41928, 63832 , 119608, 199363, 72, 140, 272, 432]
    # Create initial matrix of distance between teams
    
    print('instance,mean,std,time,bestvalue,percent')
    for j in range(0, len(filenames)):
        
        with open(filenames[j]+'.txt') as f:
            initial_matrix = []
            for line in f.readlines():
                initial_matrix.append(list(map(int, line.split())))
        f.close()
        
        total_scores = 0
        result_arr = []
        bestValue = sys.maxsize
        initial_time = time.time()
        for i in range(0, 10):
            p = mttp(initial_matrix, 3)
            initial_time = time.time()
            p.grasp(1200, 0.8)
            result_arr.append(p.getBestValue())
            if bestValue > result_arr[i]:
                bestValue = result_arr[i]
        time_mean = (time.time() - initial_time)/10
        percentage = 100*((bestValue - best_scores[j])/best_scores[j])
        sol = list(filter(lambda elem: elem != sys.maxsize, result_arr))
        
        if len(sol):
            mean = int(sum(sol)/len(sol))
        else:
            mean = 0
        if sol:
            std = np.std(sol)
        else:
            std = 0
        
        print('{},{},{},{},{},{}'.format(filenames[j], mean, std, time_mean, bestValue, percentage))


if __name__ == '__main__':
    tests()
