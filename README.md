# GRASP

## Requisitos

- Python 3.8 >=

## Biliotecas necessárias

- sys
- os
- numpy
- random
- math
- time
- itertools

## Execução

- $python3 grasp.py

ou

- $./python3 grasp.py